# Bases de Datos II - dao-inscripciones

Autores: Villagra Julieta, Soria José

## Modelo

[![Modelo de datos](https://gitlab.com/soriajose/dao-inscripciones/-/raw/main/modelo/modelo.jpeg)](https://gitlab.com/soriajose/dao-inscripciones/-/blob/main/modelo/modelo.jpeg)

## Scripts

#### Create database schema
* [dbscripts](https://gitlab.com/soriajose/dao-inscripciones/-/blob/main/scripts/create-tables.sql)
* [createuser](https://gitlab.com/soriajose/dao-inscripciones/-/blob/main/scripts/create-user.sql)
* [insertdatos](https://gitlab.com/soriajose/dao-inscripciones/-/blob/main/scripts/insert-table.sql)
* [createtrigger](https://gitlab.com/soriajose/dao-inscripciones/-/blob/main/scripts/create-trigger.sql)


#### Instalación:
* IDE o editor de texto 
* Python 3.6 or 3.7
* pip install --upgrade pip (LINUX)
* https://www.python.org/downloads/release/python-370/ (WINDOWS)
* Instalar un gestor de BD

#### Instalar Libs
* pip install sqlalchemy==1.4

#### Instalar Oracle Databases
* pip install cx_Oracle==8.3

#### Instalar jupyter
* pip install notebook 

#### Instalar la libreria de sqlalchemy
* pip install sqlalchemy==1.4

#### Instalar el kernel
* El VSCode te lo pide al instalar cuando se ejecuta desde la notebooks


#### Crear un entorno virtual (LINUX)
* python3 -m venv ./venv

#### Instalar el instant client (LINUX)
https://docs.oracle.com/en-us/iaas/autonomous-database-serverless/doc/connecting-python-tls.html
https://csiandal.medium.com/install-oracle-instant-client-on-ubuntu-4ffc8fdfda08

#### Instalar el instant client (WINDOWS)
* https://www.oracle.com/ar/database/technologies/instant-client/downloads.html
* Crear una carpeta en el disco C llamado Oracle y pegar la wallet de Oracle Cloud
* Agregar las variables de entorno