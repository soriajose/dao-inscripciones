-- Generado por Oracle SQL Developer Data Modeler 23.1.0.087.0806
--   en:        2024-04-26 18:01:10 ART
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE SEQUENCE seq_alumnos;

CREATE SEQUENCE seq_cursos;

CREATE SEQUENCE seq_establecimientos;

CREATE SEQUENCE seq_inscripciones;

CREATE SEQUENCE seq_maestros;

CREATE TABLE alumnos (
    alu_id               NUMBER NOT NULL,
    alu_nombre           VARCHAR2(20 CHAR) NOT NULL,
    alu_apellido         VARCHAR2(20 CHAR) NOT NULL,
    alu_fecha_nacimiento DATE NOT NULL,
    alu_domicilio        VARCHAR2(20 CHAR) NOT NULL,
    alu_telefono         VARCHAR2(20 CHAR) NOT NULL,
)
LOGGING;

ALTER TABLE alumnos ADD CONSTRAINT alumno_pk PRIMARY KEY ( alu_id );

CREATE TABLE cursos (
    crs_id              NUMBER NOT NULL,
    crs_anio_lectivo    VARCHAR2(40) NOT NULL,
    crs_nivel_escolar   VARCHAR2(40),
    crs_cupo_total      NUMBER,
    crs_cant_disponible NUMBER,
    crs_division        VARCHAR2(20),
    eto_id              NUMBER NOT NULL
)
LOGGING;

ALTER TABLE cursos ADD CONSTRAINT curso_pk PRIMARY KEY ( crs_id );

CREATE TABLE cursos_maestros (
    crs_id NUMBER NOT NULL,
    mae_id NUMBER NOT NULL
)
LOGGING;

CREATE TABLE establecimientos (
    eto_id        NUMBER NOT NULL,
    eto_nombre    VARCHAR2(40 CHAR),
    eto_direccion VARCHAR2(20 CHAR),
    eto_telefono  VARCHAR2(20 CHAR)
)
LOGGING;

ALTER TABLE establecimientos ADD CONSTRAINT establecimientos_pk PRIMARY KEY ( eto_id );

CREATE TABLE inscripciones (
    ins_id    NUMBER NOT NULL,
    ins_fecha DATE NOT NULL,
    crs_id    NUMBER NOT NULL,
    alu_id    NUMBER NOT NULL
)
LOGGING;

CREATE INDEX ins_crs_id_idx ON
    inscripciones (
        crs_id
    ASC )
        LOGGING;

CREATE INDEX ins_alu_id_idx ON
    inscripciones (
        alu_id
    ASC )
        LOGGING;

CREATE INDEX ins_ins_fecha_idx ON
    inscripciones (
        ins_fecha
    ASC )
        LOGGING;

ALTER TABLE inscripciones ADD CONSTRAINT inscripcion_pk PRIMARY KEY ( ins_id );

CREATE TABLE maestros (
    mae_id        NUMBER NOT NULL,
    mae_nombre    VARCHAR2(40) NOT NULL,
    mae_apellido  VARCHAR2(40) NOT NULL,
    mae_domicilio VARCHAR2(40) NOT NULL,
    mae_telefono  VARCHAR2(40) NOT NULL
)
LOGGING;

ALTER TABLE maestros ADD CONSTRAINT maestro_pk PRIMARY KEY ( mae_id );

ALTER TABLE cursos
    ADD CONSTRAINT cursos_establecimientos_fk FOREIGN KEY ( eto_id )
        REFERENCES establecimientos ( eto_id )
    NOT DEFERRABLE;

ALTER TABLE cursos_maestros
    ADD CONSTRAINT cursos_maestros_cursos_fk FOREIGN KEY ( crs_id )
        REFERENCES cursos ( crs_id )
    NOT DEFERRABLE;

ALTER TABLE cursos_maestros
    ADD CONSTRAINT cursos_maestros_maestros_fk FOREIGN KEY ( mae_id )
        REFERENCES maestros ( mae_id )
    NOT DEFERRABLE;

ALTER TABLE inscripciones
    ADD CONSTRAINT inscripciones_alumnos_fk FOREIGN KEY ( alu_id )
        REFERENCES alumnos ( alu_id )
    NOT DEFERRABLE;

ALTER TABLE inscripciones
    ADD CONSTRAINT inscripciones_cursos_fk FOREIGN KEY ( crs_id )
        REFERENCES cursos ( crs_id )
    NOT DEFERRABLE;



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             6
-- CREATE INDEX                             4
-- ALTER TABLE                             10
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          5
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
