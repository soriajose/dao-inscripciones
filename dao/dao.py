#from sqlalchemy import create_engine, select, join, MetaData, Table
import sys
import os
dirname = os.path.dirname(__file__)
 
sys.path.append(dirname)
sys.path.append(dirname+"/db_models/")
from sqlalchemy import create_engine, select, join, MetaData, Table

from db_models.maestros import Maestros
from db_models.establecimientos import Establecimientos
from db_models.cursos import Cursos
from db_models.alumnos import Alumnos
from db_models.inscripciones import Inscripciones
from config_vars import BBDD_CONNECTION


class InscripcionDAO:
    print("starting")
    engine = create_engine(BBDD_CONNECTION)
    connection = engine.connect()
    print("finished connection")
    metadata = MetaData()

    """ -------------------------------------------------- MAESTROS -------------------------------------------------- """
    def get_maestros(self, *, mae_id=None):
        if mae_id:
            query = Maestros.maestro_by_id(mae_id=mae_id)
        else:
            query = Maestros.all_maestros()
        return self.connection.execute(query).fetchall()

    def get_maestros_by_nombre_or_apellido(self, *, mae_string):
        query = Maestros.maestros_by_nombre_or_apellido(mae_string=mae_string)
        return self.connection.execute(query).fetchall()
    
    def insert_maestro(self, mae_nombre, mae_apellido, mae_domicilio, mae_telefono):
        query = Maestros.insert_maestro(mae_nombre=mae_nombre,
                                          mae_apellido=mae_apellido,
                                          mae_domicilio=mae_domicilio,
                                          mae_telefono=mae_telefono)
        return self.connection.execute(query)
        
    def update_maestro(self, mae_id, mae_nombre, mae_apellido, mae_domicilio, mae_telefono):
        query = Maestros.update_maestro(mae_id=mae_id,
                                         mae_nombre=mae_nombre,
                                          mae_apellido=mae_apellido,
                                          mae_domicilio=mae_domicilio,
                                          mae_telefono=mae_telefono)
        return self.connection.execute(query)
    
    def delete_maestro(self, *, mae_id):
        query = Maestros.delete_maestro(mae_id=mae_id)
        return self.connection.execute(query)
    
    """ -------------------------------------------------- ESTABLECIMIENTOS -------------------------------------------------- """

    def get_establecimientos(self, *, eto_id=None):
        if eto_id:
            query = Establecimientos.establecimiento_by_id(eto_id=eto_id)
        else:
            query = Establecimientos.all_establecimientos()
        return self.connection.execute(query).fetchall()

    def get_establecimiento_by_nombre(self, *, eto_nombre):
        query = Establecimientos.establecimiento_by_nombre(eto_nombre=eto_nombre)
        return self.connection.execute(query).fetchall()
    
    def insert_establecimiento(self, eto_nombre, eto_direccion, eto_telefono):
        query = Establecimientos.insert_establecimiento(eto_nombre=eto_nombre,
                                                        eto_direccion=eto_direccion,
                                                        eto_telefono=eto_telefono)
        return self.connection.execute(query)
        
    def update_establecimiento(self, eto_id, eto_nombre, eto_direccion, eto_telefono):
         query = Establecimientos.update_establecimiento(eto_id=eto_id,
                                                         eto_nombre=eto_nombre,
                                                         eto_direccion=eto_direccion,
                                                         eto_telefono=eto_telefono)
         return self.connection.execute(query)
    
    def delete_establecimiento(self, *, eto_id):
        query = Establecimientos.delete_establecimiento(eto_id=eto_id)
        return self.connection.execute(query)
    
    """ -------------------------------------------------- CURSOS -------------------------------------------------- """

    def get_cursos(self, *, crs_id=None):
        if crs_id:
            query = Cursos.curso_by_id(crs_id=crs_id)
        else:
            query = Cursos.all_cursos()
        return self.connection.execute(query).fetchall()
    
    def insert_curso(self, *, crs_anio_lectivo, crs_nivel_escolar, crs_cupo_total, crs_cant_disponible, crs_division, eto_id):
        query = Cursos.insert_curso(crs_anio_lectivo=crs_anio_lectivo, 
                                    crs_nivel_escolar=crs_nivel_escolar,
                                    crs_cupo_total=crs_cupo_total,
                                    crs_cant_disponible=crs_cant_disponible,
                                    crs_division=crs_division, 
                                    eto_id=eto_id)
        return self.connection.execute(query)
    
    def update_curso(self, *, crs_id, crs_anio_lectivo, crs_nivel_escolar, crs_cupo_total, crs_cant_disponible, crs_division, eto_id):
        query = Cursos.update_curso(crs_id=crs_id,
                                    crs_anio_lectivo=crs_anio_lectivo, 
                                    crs_nivel_escolar=crs_nivel_escolar,
                                    crs_cupo_total=crs_cupo_total,
                                    crs_cant_disponible=crs_cant_disponible,
                                    crs_division=crs_division, 
                                    eto_id=eto_id)
        return self.connection.execute(query)
    
    def delete_curso(self, *, crs_id):
        query = Cursos.delete_curso(crs_id=crs_id)
        return self.connection.execute(query)
    
    """ -------------------------------------------------- ALUMNOS -------------------------------------------------- """

    def get_alumnos(self, *, alu_id=None):
        if alu_id:
            query = Alumnos.single_alumno(alu_id=alu_id)
        else:
            query = Alumnos.all_alumnos()
        return self.connection.execute(query).fetchall()
    
    
    def get_alumnos_by_nombre_or_apellido(self, *, alu_string):
        query = Alumnos.alumnos_by_nombre_or_apellido(alu_string = alu_string)
        return self.connection.execute(query).fetchall()
    
        
    def insert_alumnos(self, alu_nombre, alu_apellido, alu_fecha_nacimiento, alu_domicilio, alu_telefono):
        query = Alumnos.insert_alumnos(alu_nombre, alu_apellido, alu_fecha_nacimiento, alu_domicilio, alu_telefono)
        return self.connection.execute(query)
        
    def update_alumnos(self,alu_id, alu_nombre, alu_apellido, alu_fecha_nacimiento, alu_domicilio,alu_telefono):
        query = Alumnos.update_alumno(alu_id,alu_nombre, alu_apellido, alu_fecha_nacimiento, alu_domicilio,alu_telefono )
        return self.connection.execute(query)

    def delete_alumnos(self, alu_id):
        query = Alumnos.delete_alumnos(alu_id)
        return self.connection.execute(query)
    
    """ -------------------------------------------------- INSCRIPCIONES -------------------------------------------------- """
    def get_inscripciones(self, *, ins_id=None):
        if ins_id:
            query = Inscripciones.single_inscripciones(ins_id=ins_id)
        else:
            query = Inscripciones.all_inscripciones()
        return self.connection.execute(query).fetchall()
    
    def insert_inscripciones(self, ins_fecha, crs_id, alu_id):
        query = Inscripciones.insert_inscripcion(ins_fecha, crs_id, alu_id)
        return self.connection.execute(query)
        
    def update_inscripciones(self,ins_id, ins_fecha, crs_id, alu_id):
        query = Inscripciones.update_inscripciones(ins_id, ins_fecha, crs_id, alu_id)
        return self.connection.execute(query)

    def delete_inscripciones(self, ins_id):
        query = Inscripciones.delete_inscripciones(ins_id)
        return self.connection.execute(query)