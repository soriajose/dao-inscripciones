from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, or_
import datetime

from config_vars import BBDD_CONNECTION

Base = declarative_base()


class Inscripciones(Base):
    __tablename__ = "inscripciones"
    print("cargando configuracion de inscripciones")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    inscripciones = Table("inscripciones", metadata, autoload=True, autoload_with=engine, schema='inscripcion')

    id_not_in_db = Column(Integer, primary_key=True)
    print("configuracion de inscripciones finalizada")
    
    @classmethod
    def all_inscripciones(cls):
        """
            Devolver todas las inscripciones
        """
        query = select([cls.inscripciones])
        return query
    
    @classmethod
    def single_inscripciones(cls,*,ins_id):
        """
            #Devolver una inscripcion
        """
        query = select([cls.inscripciones]).where(cls.inscripciones.c.ins_id == ins_id)
        return query
        
    #ingresar nueva inscripcion
    @classmethod
    def insert_inscripcion(cls, ins_fecha, crs_id, alu_id):
        fecha_inscripcion = datetime.datetime.strptime(ins_fecha, '%d-%m-%Y').date()
        insert_query = cls.inscripciones.insert().values(
            ins_fecha=fecha_inscripcion,
            crs_id=crs_id,
            alu_id=alu_id)
        return insert_query
      
    #actulizar inscripcion    
    @classmethod
    def update_inscripcion(cls,ins_id, ins_fecha, crs_id, alu_id):
        update_query = cls.alumnos.update().where(cls.alumnos.c.alu_id == alu_id).values(
            ins_id=ins_id,
            ins_fecha=ins_fecha,
            crs_id=crs_id,
            alu_id=alu_id)
        return update_query
    
    #eliminar inscripcion
    @classmethod
    def delete_inscripciones(cls,ins_id):
        delete_query = cls.inscripciones.delete().where(cls.inscripciones.c.ins_id == ins_id)
        return delete_query