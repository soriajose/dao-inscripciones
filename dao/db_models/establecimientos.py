from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, or_, Sequence, String, Numeric

from config_vars import BBDD_CONNECTION

Base = declarative_base()


class Establecimientos(Base):
    __tablename__ = "establecimientos"

    print("Inicio la configuración de la tabla establecimientos")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    establecimientos = Table("establecimientos", metadata, autoload=True, autoload_with=engine, schema='inscripcion')
    eto_id = Column(Integer, Sequence('seq_establecimientos', schema='inscripcion'), primary_key=True)
    eto_nombre = Column(String(255))
    eto_direccion = Column(String(255))
    eto_telefono = Column(String(255))
    print("Finalizo la configuración de la tabla establecimientos")

    @classmethod
    def all_establecimientos(cls):
        query = select([cls.establecimientos])
        return query
    
    @classmethod
    def establecimiento_by_id(cls, *, eto_id):
        query = select([cls.establecimientos]).where(cls.establecimientos.c.eto_id == eto_id)
        return query
    
    @classmethod
    def establecimiento_by_nombre(cls, *, eto_nombre):
        query = select([cls.establecimientos]).where(cls.establecimientos.c.eto_nombre == eto_nombre)
        return query
    
    @classmethod
    def insert_establecimiento(cls, eto_nombre, eto_direccion, eto_telefono):
        insert = cls.establecimientos.insert().values(
            eto_nombre=eto_nombre,
            eto_direccion=eto_direccion,
            eto_telefono=eto_telefono)
        return insert
    
    @classmethod
    def update_establecimiento(cls, eto_id, eto_nombre, eto_direccion, eto_telefono):
        update = cls.establecimientos.update().where(cls.establecimientos.c.eto_id == eto_id).values(
            eto_nombre=eto_nombre,
            eto_direccion=eto_direccion,
            eto_telefono=eto_telefono)
        return update
    
    @classmethod
    def delete_establecimiento(cls, eto_id):
        delete = cls.establecimientos.delete().where(cls.establecimientos.c.eto_id == eto_id)
        return delete