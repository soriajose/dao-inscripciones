from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, or_

from config_vars import BBDD_CONNECTION

Base = declarative_base()


class Maestros(Base):
    __tablename__ = "maestros"

    print("Inicio la configuración de la tabla maestros")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    maestros = Table("maestros", metadata, autoload=True, autoload_with=engine, schema='inscripcion')
    mae_id = Column(Integer, primary_key=True)
    print("Finalizo la configuración de la tabla maestros")

    @classmethod
    def all_maestros(cls):
        query = select([cls.maestros])
        return query
    
    @classmethod
    def maestro_by_id(cls, *, mae_id):
        query = select([cls.maestros]).where(cls.maestros.c.mae_id == mae_id)
        return query
    
    @classmethod
    def maestros_by_nombre_or_apellido(cls, *, mae_string):
        query = select([cls.maestros]).where(or_( cls.maestros.c.mae_nombre.like(f'%{mae_string}%'), cls.maestros.c.mae_apellido.like(f'%{mae_string}%')))
        return query
    
    @classmethod
    def insert_maestro(cls, mae_nombre, mae_apellido, mae_domicilio, mae_telefono):
        insert = cls.maestros.insert().values(
            mae_nombre=mae_nombre,
            mae_apellido=mae_apellido,
            mae_domicilio=mae_domicilio,
            mae_telefono=mae_telefono)
        return insert
    
    @classmethod
    def update_maestro(cls, mae_id, mae_nombre, mae_apellido, mae_domicilio, mae_telefono):
        update = cls.maestros.update().where(cls.maestros.c.mae_id == mae_id).values(
            mae_nombre=mae_nombre,
            mae_apellido=mae_apellido,
            mae_domicilio=mae_domicilio,
            mae_telefono=mae_telefono)
        return update
    
    @classmethod
    def delete_maestro(cls, mae_id):
        delete = cls.maestros.delete().where(cls.maestros.c.mae_id == mae_id)
        return delete