from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, or_
import datetime

from config_vars import BBDD_CONNECTION

Base = declarative_base()


class Alumnos(Base):
    __tablename__ = "alumnos"

    print("Inicio la configuración de la tabla alumnos")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    alumnos = Table("alumnos", metadata, autoload=True, autoload_with=engine, schema='inscripcion')
    alu_id = Column(Integer, primary_key=True)
    print("Finalizo la configuración de la tabla alumnos")

    @classmethod
    def all_alumnos(cls):
        """
            Devolver todos los Alumnos
        """
        query = select([cls.alumnos])
        return query
    
    @classmethod
    def single_alumno(cls,*,alu_id):
        """
            Devolver un Alumno
        """
        query = select([cls.alumnos]).where(cls.alumnos.c.alu_id == alu_id)
        return query
    
    #realizar consultas a la tabla de alumnos buscando coincidencias parciales en el nombre o apellido del alumno y devuelve la consulta SQL
    @classmethod
    def alumnos_by_nombre_or_apellido(cls,*,alu_string):
        query = select([cls.alumnos]).where(or_( cls.alumnos.c.alu_nombre.like(f'%{alu_string}%'), cls.alumnos.c.alu_apellido.like(f'%{alu_string}%')))
        return query
    
    #ingreso alumnos
    @classmethod
    def insert_alumnos(cls, alu_nombre, alu_apellido, alu_fecha_nacimiento, alu_domicilio, alu_telefono):
        # insertar un nuevo alumno
        fecha_nacimiento = datetime.datetime.strptime(alu_fecha_nacimiento, '%d-%m-%Y').date()
        insert_query = cls.alumnos.insert().values(
            alu_nombre=alu_nombre,
            alu_apellido=alu_apellido,
            alu_fecha_nacimiento=fecha_nacimiento,
            alu_domicilio=alu_domicilio,
            alu_telefono= alu_telefono)
        return insert_query
    
    #actulizar alumno        
    @classmethod
    def update_alumno(cls, alu_id, alu_nombre, alu_apellido,alu_fecha_nacimiento, alu_domicilio, alu_telefono):
        fecha_nacimiento = datetime.datetime.strptime(alu_fecha_nacimiento, '%d-%m-%Y').date()
        update_query = cls.alumnos.update().where(cls.alumnos.c.alu_id == alu_id).values(
            alu_nombre=alu_nombre,
            alu_apellido=alu_apellido,
            alu_fecha_nacimiento=fecha_nacimiento,
            alu_domicilio=alu_domicilio,
            alu_telefono= alu_telefono)
        return update_query
    
    
    #eliminamor un alumno
    @classmethod
    def delete_alumnos(cls, alu_id):
        delete_query = cls.alumnos.delete().where(cls.alumnos.c.alu_id == alu_id)
        return delete_query