from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, select, join, MetaData, Table, or_

from config_vars import BBDD_CONNECTION

Base = declarative_base()


class Cursos(Base):
    __tablename__ = "cursos"

    print("Inicio la configuración de la tabla cursos")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    cursos = Table("cursos", metadata, autoload=True, autoload_with=engine, schema='inscripcion')
    id_not_in_db = Column(Integer, primary_key=True)
    print("Finalizo la configuración de la tabla cursos")

    @classmethod
    def all_cursos(cls):
        query = select([cls.cursos])
        return query
    
    @classmethod
    def curso_by_id(cls, *, crs_id):
        query = select([cls.cursos]).where(cls.cursos.c.crs_id == crs_id)
        return query
    
    @classmethod
    def insert_curso(cls, crs_anio_lectivo, crs_nivel_escolar, crs_cupo_total, crs_cant_disponible, crs_division, eto_id):
        insert = cls.cursos.insert().values(
            crs_anio_lectivo=crs_anio_lectivo,
            crs_nivel_escolar=crs_nivel_escolar,
            crs_cupo_total=crs_cupo_total,
            crs_cant_disponible=crs_cant_disponible,
            crs_division=crs_division,
            eto_id=eto_id)
        return insert
    
    @classmethod
    def update_curso(cls, crs_id, crs_anio_lectivo, crs_nivel_escolar, crs_cupo_total, crs_cant_disponible, crs_division, eto_id):
        update = cls.cursos.update().where(cls.cursos.c.crs_id == crs_id).values(
             crs_anio_lectivo=crs_anio_lectivo,
            crs_nivel_escolar=crs_nivel_escolar,
            crs_cupo_total=crs_cupo_total,
            crs_cant_disponible=crs_cant_disponible,
            crs_division=crs_division,
            eto_id=eto_id)
        return update
    
    @classmethod
    def delete_curso(cls, crs_id):
        delete = cls.cursos.delete().where(cls.cursos.c.crs_id == crs_id)
        return delete